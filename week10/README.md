# Week 10: Hackathon 

Build some stuff.

Like anything your heart desires... go make big brother, poor man's Nest, etc.

Feel free to use a mix of anything you learned in this course, as well as the previous courses.

Here are some of technologies we used that you can try, or even dig deeper on!
- IoT Hub
- IoT Devices and IoT Edge Devices
- Azure Machine Learning
- Logic Apps
- Web Apps
- Containers
- Azure Container Instance
- Cognitive Services (try Speech, Text and more)
- Use another cloud provider service adn integrate it.

Other tutorials to help you along the way:

- You could add more functionality to the "Me or Not?" App in Week 8/9. In the original source code [https://github.com/Azure-Samples/iotedge-scott-or-not](https://github.com/Azure-Samples/iotedge-scott-or-not) there was information about how to integrate Logic Apps and add an Azure Functions Module for the Edge using C#.
- Read more about how [IoT Edge modules interact](https://docs.microsoft.com/en-us/azure/iot-edge/module-composition) in order to build extricate module-to-module communication via message routing. Remember, you can also talk to other containers since you're on the same network when modules are deployed to same Edge.
- Convert IoT Edge to a gateway to talk to downstream IoT devices
- Do notifications with [IoT remote monitoring with Logic Apps](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-monitoring-notifications-with-azure-logic-apps)