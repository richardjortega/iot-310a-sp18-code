# Week 9: Visualization, ML/Streaming Analytics, and End-to-End Integration

## Setup a module to send sensor data to IoT Hub

The following guides are going to expect humidity and temperature information from your Raspberry Pi's SenseHAT. Let's go ahead and use a containerized version of our Python app that read temperature and humidity from the SenseHAT.

Start the container (we won't deploy it as a module yet):

```bash
pi$ docker pull richardjortega/sensetemp:arm32v7
pi$ docker tag richardjortega/sensetemp:arm32v7 sensetemp
pi$ docker run -d sensetemp
```


## Visualize real-time sensor data from your Azure IoT hub by using the Web Apps feature of Azure App Service

![End-to-end diagram](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-get-started-e2e-diagram/5.png)

### What you learn

We'll visualize real-time sensor data that your IoT hub receives by running a web application that is hosted on a web app. 

### What you do

- Create a web app in the Azure portal.
- Get your IoT hub ready for data access by adding a consumer group.
- Configure the web app to read sensor data from your IoT hub.
- Upload a web application to be hosted by the web app.
- Open the web app to see real-time temperature and humidity data from your IoT hub.

### Create a web app

1. In the [Azure portal](https://portal.azure.com/), click **Create a resource** > **Web + Mobile** > **Web App**.
2. Enter a unique job name, verify the subscription, specify a resource group and a location, select **Pin to dashboard**, and then click **Create**.

   We recommend that you select the same location as that of your resource group. Doing so assists with processing speed and reduces the cost of data transfer.

   ![Create a web app](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/2_create-web-app-azure.png)

### Add a consumer group to your IoT hub

Consumer groups are used by applications to pull data from Azure IoT Hub. In this tutorial, you create a consumer group to be used by a coming Azure service to read data from your IoT hub.

To add a consumer group to your IoT hub, follow these steps:

1. In the [Azure portal](https://ms.portal.azure.com/), open your IoT hub.
2. In the left pane, click **Endpoints**, select **Events** on the middle pane, enter a name under **Consumer groups** on the right pane, and then click **Save**.

   ![Create a consumer group in your IoT hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-create-consumer-group/1_iot-hub-create-consumer-group-azure.png)

### Configure the web app to read data from your IoT hub

1. Open the web app you’ve just provisioned.
2. Click **Application settings**, and then, under **App settings**, add the following key/value pairs:

   | Key                                   | Value                                                        |
   |---------------------------------------|--------------------------------------------------------------|
   | Azure.IoT.IoTHub.ConnectionString     | Obtained from iothub-explorer                                |
   | Azure.IoT.IoTHub.ConsumerGroup        | The name of the consumer group that you add to your IoT hub  |

   ![Add settings to your web app with key/value pairs](https://github.com/MicrosoftDocs/azure-docs/raw/master/articles/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/4_web-app-settings-key-value-azure.png)

3. Click **Application settings**, under **General settings**, toggle the **Web sockets** option, and then click **Save**.

   ![Toggle the Web sockets option](https://github.com/MicrosoftDocs/azure-docs/raw/master/articles/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/10_toggle_web_sockets.png)

### Upload a web application to be hosted by the web app

On GitHub, we've made available a web application that displays real-time sensor data from your IoT hub. All you need to do is configure the web app to work with a Git repository, download the web application from GitHub, and then upload it to Azure for the web app to host.

1. In the web app, click **Deployment Options** > **Choose Source** > **Local Git Repository**, and then click **OK**.

   ![Configure your web app deployment to use the local Git repository](https://github.com/MicrosoftDocs/azure-docs/raw/master/articles/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/5_configure-web-app-deployment-local-git-repository-azure.png)

2. Click **Deployment Credentials**, create a user name and password to use to connect to the Git repository in Azure, and then click **Save**.

3. Click **Overview**, and note the value of **Git clone url**.

   ![Get the Git clone URL of your web app](https://github.com/MicrosoftDocs/azure-docs/raw/master/articles/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/7_web-app-git-clone-url-azure.png)

4. Open a command or terminal window on your local computer.

5. Download the web app from GitHub, and upload it to Azure for the web app to host. To do so, run the following commands:

   ```bash
   git clone https://github.com/Azure-Samples/web-apps-node-iot-hub-data-visualization.git
   cd web-apps-node-iot-hub-data-visualization
   git remote add webapp <Git clone URL>
   git push webapp master:master
   ```

**NOTE:** \<Git clone URL\> is the URL of the Git repository found on the **Overview** page of the web app.

### Open the web app to see real-time temperature and humidity data from your IoT hub

On the **Overview** page of your web app, click the URL to open the web app.

![Get the URL of your web app](https://github.com/MicrosoftDocs/azure-docs/raw/master/articles/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/8_web-app-url-azure.png)

You should see the real-time temperature and humidity data from your IoT hub.

![Web app page showing real-time temperature and humidity](https://github.com/MicrosoftDocs/azure-docs/raw/master/articles/iot-hub/media/iot-hub-live-data-visualization-in-web-apps/9_web-app-page-show-real-time-temperature-humidity-azure.png)

**NOTE**: Ensure the sample application is running on your device. If not, you will get a blank chart, you can refer to the tutorials under [Setup your device](iot-hub-raspberry-pi-kit-node-get-started.md).

You've successfully used your web app to visualize real-time sensor data from your IoT hub.

---
## Weather forecast using the sensor data from your IoT hub in Azure Machine Learning

![End-to-end diagram](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-get-started-e2e-diagram/6.png)

**NOTE: Azure Stream Analytics (ASA) is a relatively expensive service, after running the demo, please delete your resources**

Machine learning is a technique of data science that helps computers learn from existing data to forecast future behaviors, outcomes, and trends.  We'll use Azure Machine Learning to do weather forecast (chance of rain) using the temperature and humidity data from your Azure IoT hub. The chance of rain is the output of a prepared weather prediction model. The model is built upon historic data to forecast chance of rain based on temperature and humidity.

**NOTE**: This guide uses an older, but great for begineers, application called "Azure ML Studio". It is no longer part of the Azure Machine Learning apps, but the latest tools like Azure Machine Leanring Workbench please see [Azure Machine Learning Overview](https://docs.microsoft.com/en-us/azure/machine-learning/service/overview-what-is-azure-ml).  

### What you do

- Deploy the weather prediction model as a web service.
- Get your IoT hub ready for data access by adding a consumer group.
- Create a Stream Analytics job and configure the job to:
  - Read temperature and humidity data from your IoT hub.
  - Call the web service to get the rain chance.
  - Save the result to an Azure blob storage.
- Use Microsoft Azure Storage Explorer to view the weather forecast.

### Deploy the weather prediction model as a web service

1. Go to the [weather prediction model page](https://gallery.cortanaintelligence.com/Experiment/Weather-prediction-model-1).
1. Click **Open in Studio** in Microsoft Azure Machine Learning Studio.
   ![Open the weather prediction model page in Cortana Intelligence Gallery](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/2_weather-prediction-model-in-cortana-intelligence-gallery.png)
1. Click **Run** to validate the steps in the model. This step might take 2 minutes to complete.
   ![Open the weather prediction model in Azure Machine Learning Studio](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/3_open-weather-prediction-model-in-azure-machine-learning-studio.png)
1. Click **SET UP WEB SERVICE** > **Predictive Web Service**.
   ![Deploy the weather prediction model in Azure Machine Learning Studio](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/4-deploy-weather-prediction-model-in-azure-machine-learning-studio.png)
1. In the diagram, drag the **Web service input** module somewhere near the **Score Model** module.
1. Connect the **Web service input** module to the **Score Model** module.
   ![Connect two modules in Azure Machine Learning Studio](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/13_connect-modules-azure-machine-learning-studio.png)
1. Click **RUN** to validate the steps in the model.
1. Click **DEPLOY WEB SERVICE** to deploy the model as a web service.
1. On the dashboard of the model, download the **Excel 2010 or earlier workbook** for **REQUEST/RESPONSE**.

   > [!Note]
   > Ensure that you download the **Excel 2010 or earlier workbook** even if you are running a later version of Excel on your computer.

   ![Download the Excel for the REQUEST RESPONSE endpoint](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/5_download-endpoint-app-excel-for-request-response.png)

1. Open the Excel workbook, make a note of the **WEB SERVICE URL** and **ACCESS KEY**.

## Add a consumer group to your IoT hub

Consumer groups are used by applications to pull data from Azure IoT Hub. In this tutorial, you create a consumer group to be used by a coming Azure service to read data from your IoT hub.

To add a consumer group to your IoT hub, follow these steps:

1. In the [Azure portal](https://ms.portal.azure.com/), open your IoT hub.
2. In the left pane, click **Endpoints**, select **Events** on the middle pane, enter a name under **Consumer groups** on the right pane, and then click **Save**.

   ![Create a consumer group in your IoT hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-create-consumer-group/1_iot-hub-create-consumer-group-azure.png)

### Create, configure, and run a Stream Analytics job

#### Create a Stream Analytics job

1. In the [Azure portal](https://portal.azure.com/), click **Create a resource** > **Internet of Things** > **Stream Analytics job**.
1. Enter the following information for the job.

   **Job name**: The name of the job. The name must be globally unique.

   **Resource group**: Use the same resource group that your IoT hub uses.

   **Location**: Use the same location as your resource group.

   **Pin to dashboard**: Check this option for easy access to your IoT hub from the dashboard.

   ![Create a Stream Analytics job in Azure](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/7_create-stream-analytics-job-azure.png)

1. Click **Create**.

#### Add an input to the Stream Analytics job

1. Open the Stream Analytics job.
1. Under **Job Topology**, click **Inputs**.
1. In the **Inputs** pane, click **Add**, and then enter the following information:

   **Input alias**: The unique alias for the input.

   **Source**: Select **IoT hub**.

   **Consumer group**: Select the consumer group you created.

   ![Add an input to the Stream Analytics job in Azure](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/8_add-input-stream-analytics-job-azure.png)

1. Click **Create**.

#### Add an output to the Stream Analytics job

1. Under **Job Topology**, click **Outputs**.
1. In the **Outputs** pane, click **Add**, and then enter the following information:

   **Output alias**: The unique alias for the output.

   **Sink**: Select **Blob Storage**.

   **Storage account**: The storage account for your blob storage. You can create a storage account or use an existing one.

   **Container**: The container where the blob is saved. You can create a container or use an existing one.

   **Event serialization format**: Select **CSV**.

   ![Add an output to the Stream Analytics job in Azure](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/9_add-output-stream-analytics-job-azure.png)

1. Click **Create**.

#### Add a function to the Stream Analytics job to call the web service you deployed

1. Under **Job Topology**, click **Functions** > **Add**.
1. Enter the following information:

   **Function Alias**: Enter `machinelearning`.

   **Function Type**: Select **Azure ML**.

   **Import option**: Select **Import from a different subscription**.

   **URL**: Enter the WEB SERVICE URL that you noted down from the Excel workbook.

   **Key**: Enter the ACCESS KEY that you noted down from the Excel workbook.

   ![Add a function to the Stream Analytics job in Azure](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/10_add-function-stream-analytics-job-azure.png)

1. Click **Create**.

#### Configure the query of the Stream Analytics job

1. Under **Job Topology**, click **Query**.
1. Replace the existing code with the following code:

   ```sql
   WITH machinelearning AS (
      SELECT EventEnqueuedUtcTime, temperature, humidity, machinelearning(temperature, humidity) as result from [YourInputAlias]
   )
   Select System.Timestamp time, CAST (result.[temperature] AS FLOAT) AS temperature, CAST (result.[humidity] AS FLOAT) AS humidity, CAST (result.[Scored Probabilities] AS FLOAT ) AS 'probabalities of rain'
   Into [YourOutputAlias]
   From machinelearning
   ```

   Replace `[YourInputAlias]` with the input alias of the job.

   Replace `[YourOutputAlias]` with the output alias of the job.

1. Click **Save**.

#### Run the Stream Analytics job

In the Stream Analytics job, click **Start** > **Now** > **Start**. Once the job successfully starts, the job status changes from **Stopped** to **Running**.

![Run the Stream Analytics job](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/11_run-stream-analytics-job-azure.png)

### Use Microsoft Azure Storage Explorer to view the weather forecast

Run the client application to start collecting and sending temperature and humidity data to your IoT hub. For each message that your IoT hub receives, the Stream Analytics job calls the weather forecast web service to produce the chance of rain. The result is then saved to your Azure blob storage. Azure Storage Explorer is a tool that you can use to view the result.

1. [Download and install Microsoft Azure Storage Explorer](http://storageexplorer.com/).
1. Open Azure Storage Explorer.
1. Sign in to your Azure account.
1. Select your subscription.
1. Click your subscription > **Storage Accounts** > your storage account > **Blob Containers** > your container.
1. Open a .csv file to see the result. The last column records the chance of rain.

   ![Get weather forecast result with Azure Machine Learning](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-weather-forecast-machine-learning/12_get-weather-forecast-result-azure-machine-learning.png)

## Summary

You’ve successfully used Azure Machine Learning to produce the chance of rain based on the temperature and humidity data that your IoT hub receives.

## End-to-End Integration

Rest of the class will be a discussion in preparation for Week 10, The Hackathon.