# Get images if not downloaded
# Requires: docker
docker pull richardjortega/iotedgectl
docker tag richardjortega/iotedgectl iotedgectl

docker run -it --privileged \
               -v /var/run/docker.sock:/var/run/docker.sock \
               iotedgectl bash
               