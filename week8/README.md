# Week 8: Raspberry Pi Camera, IoT Edge, Cognitive Services, Me or Not? App

To ensure everyone is on the same page, the following is provided to help recreate your IoT Hub, the following are the prerequisites:

Currently we should have a resource group that contains the following:
- IoT Hub (F1 plan)
- Storage Accounts (for Cloud Shell)

Please delete the following resources for now, since they charge monthly and not based on consumption:
- CosmosDB
- Virtual Machines
- Azure Container Instances

**NOTE**: In case you need a fresh start (or are missing an IoT Hub), you can use the following OPTIONAL commands. Run them using Azure Cloud Shell or your local Azure CLI.

```bash
# Create a Resource Group
az group create --name myResourceGroup --location westus

# Create a new IoT Hub (using F1/Free SKU) -- provided you are not using any other free IoT Hub
az iot hub create --name <IOT_HUB_NAME> --resource-group myResourceGroup --sku F1
```

## Overview

Here's what we will be building today!

- Section 1: Setting up the Pi Camera
  - Setup Pi Camera
  - Using Pi Camera in Terminal
  - Using Pi Camera in Python
- Section 2: Setting up an IoT Edge Devices
  - Register IoT Edge Device on IoT Hub
  - Install Docker on the Pi
  - Install IoT Edge on Pi
- Section 3: Using Computer Vision (from Cognitive Services)
  - Quick Overview of Computer Vision
  - Train a Computer Vision ML model
  - Download and Deploy ML Model
- Section 4: Wiring it up!
  - Deploy and run Me or Not? App
- Bonus Section: Pi Camera Stuff

![](./assets/week8-overview.png)

## Section 1: Using the Pi Camera

We will focus on using the RPi Camera v2 [https://www.raspberrypi.org/products/camera-module-v2/](https://www.raspberrypi.org/products/camera-module-v2/), we will show how to take photos and videos in terminal and in Python. Later we'll put them in containers.

### Setup the RPi Camera

Copyright photos: [RaspberryPi.org](http://www.raspberrypi.org)

Making sure your RPi is turned off and using the photo below as a reference, find the CSI port. Unlock the CSI port and put the camera module ribbon as shown below (blue part facing the ethernet port). 

![](./assets/connect-camera.jpg)

Boot the RPi and update the configuration to enable the camera. You can do this with the desktop using a keyboard/monitor. Alternatively, if you don't have a screen and are SSH'ed into the Pi you can run `pi$ sudo  raspi-config` for similar settings.

Open from the main menu:

- Preferences > Raspberry Pi Configuration

![](./assets/raspi-config-menu.png)

From the menu that opens: 

- Interfaces > Set **Camera** to **enabled**

![](./assets/raspi-config.png)

Now **reboot** your RPi and your camera will be ready for use.

### Using RPi from Terminal

Before we get engaged with the RPi Camera in code, let's use some CLI tools that can help us get a feel for the camera. 

We're going to use `raspistill` and `raspivid` to get a feel for our new camera. These should already be installed on your RPi with a Raspbian install.

Alternatively, if you have a USB webcam you can use the `fswebcam` and `avconv` by using:

```bash
## Only for USB webcams
# pi$ sudo apt-get install fswebcam
# pi$ sudo apt-get install avconv

# Use "fswebcam" for still to replace "raspistill"
# Use "avconv" to replace "raspivid"
#   Usage: avconv -t -10 -f video4linux2 -i /dev/video0 video.avi
```

Let's take a simple photo! I would advise using your RPi monitor connected to review images we save. However if you have syncing with the RPi you can load up the photos/videos locally on your host machine.

#### Create a directory to store your work

```bash
~/week8:pi$ mkdir images
~/week8:pi$ cd images 
```

#### Take a photo and output to a file

```bash
~/week8/images:pi$ sudo raspistill --output img1.jpg 
```

Awesome, the file will be located at `img1.jpg` within your present working directory (i.e. `~/week8/images`). Notice that with the `-o` option we specified the output file name. You can change the name to anything you want for future photos. 

The camera module (v1) can take 5 megapixel photos at up to `2592 x 1944`, the camera module (v2) can take 8 megapixel photos at up to `3280 × 2464`. Most images will be about 3 MB in file size.

There are plenty of options you can add to `raspistill` command that are helpful. 

Reference the full list of options by typing `pi$ raspistill` with no arguments. Notice that most commands in a CLI can be shortened.

The next command will use a few in combination to demonstrate.

#### Take photo with advanced options

The following command will take a photo in 3 seconds, rotate the camera to 270 degrees, and output the photo to a file in the directory as `img_<TIMESTAMP>.jpg`

**Note**: Adjust the value of `--rotation` option to suit your camera's placement. It may not even need it.

```bash
~/week8/images:pi$ sudo raspistill --rotation 270 --output img_%d.jpg --timestamp --timeout 3000
```

Let's break down the command:

- `raspistill`:  the CLI (command line interface) that interfaces to the camera.
- `--rotation 270`: specifies a degree to rotate the camera, in this case 270 degress because my camera was titled the other way, so I needed to compensate. There are other commands that are more simple if you need to compensate like `-vf` or `-hf` for vertical flip or horizontal flip, respectively.
- `--output img_%d.jpg`: defines a filename to save to disk, in this case we've add an output format that other options can use (e.g. `--timestamp` or `--datetime`)
- `--timestamp`: replace the output pattern (`%d`) with Unix timestamp (seconds since epoch).
    - Alternatively, you can switch this to `--datetime` to human readable time.
- `--timeout`: Time (in milliseconds) before the camera module takes a picture and shuts down the application (if not specified, the default is 5 seconds). In this case, we have set it to 3000 milliseconds (or 3 seconds).
    - You can supplement this with `--keypress` if you want user input to fire the command. The camera is run for the requested time (`--timeout`), and a capture can be initiated throughout that time by pressing the `Enter` key. Pressing `X` then `Enter` will exit the application before the timeout is reached. If the timeout is set to `0`, the camera will run indefinitely until the user presses `X` then `Enter`. Using the verbose option (`-v`) will display a prompt asking for user input, otherwise no prompt is displayed.

#### Take multiple photos over a time period

The following command will take take 4 photos over a 20 second period and output each file as `img_<FRAME_COUNT_NUMBER>.jpg`. Let's 

```bash
~/week8/images:pi$ sudo raspistill --rotation 270 --output  img_%03d.jpg --timelapse 5000 --timeout 20000 
```

The `%03d` is a custom formatted date/time that says we expect a 3 digit number and we want leading zeroes to fill up empty places. For a full reference of what's accepted, you can view: [strftime - format date and time](http://man7.org/linux/man-pages/man3/strftime.3.html)

The option `--timelapse` allows us to take a photo at an interval we set in milliseconds, so in this case, we are taking a photo every 5 seconds (or 5000 ms).

Within your folder you should see: `img_001.jpg`, `img_002.jpg`, ... and so forth. There will also be a `img_000.jpg` for some reason I can't quite figure out!

#### Take video

Just like we did with `raspistill` we can use `raspivid` command to record video and save it to our RPi. 

Let's make a new folder under `week8` called `videos`

```bash
~/week8:pi$ mkdir videos
~/week8:pi$ cd videos
```

Now we can make our first video! We'll record for 10 seconds, output to a file, and put the preview mode to full screen (while rotating to fit my camera -- change according to your camera setup).

```bash
# with a rotation
~/week8/videos:pi$ sudo raspivid --output video_1.h264 --timeout 10000 --fullscreen --rotation 270

# without rotation
~/week8/videos:pi$ sudo raspivid --output video_1.h264 --timeout 10000 --fullscreen
```

To play the video, we'll use another CLI utility called `omxplayer`.


```bash
~/week8/videos:pi$ omxplayer video_1.h264
```

The video should play, it may be a little faster than when it was recorded because `omxplayer`'s fast frame rate.


## Python examples using Raspberry Pi Camera

Great, we have experimented with the camera, let's go ahead and use the RPi Camera in Python using the `python-picamera` library.

Ensure the `picamera` Python module is installed on your system: 

```bash
pi$ sudo apt-get update && sudo apt-get install python-picamera
```

### Camera Preview (Python)

The following script will show a camera preview, demonstrates how to call the Pi Camera from within a Python script using the `picamera` module.

```bash
pi$ sudo python camera_preview_pi.py
```

This will show us the preview of the camera for 20 seconds. This only works if you have a monitor connected. There are some black magic guides to using a [combo of SSH + VNC](http://42bots.com/tutorials/access-raspberry-pi-terminal-and-desktop-remotely-with-ssh-and-vnc/) to get it to view within your host machine. I couldn't get those to work but you're welcome to experiment.

If you needed to do a rotation because of how your camera is setup, you can modify the line that has `camera.rotation` and set a value for rotation.

### Camera Photo Capture (Python)

The following script will set a rotation to "270", start a preview, timestamp an image filename, and then take a photo and write to that filename.

```bash
pi$ sudo python camera_photo_pi.py
```

### Camera Video Capture (Python)

The following code will record 10 seconds of video and then close the program, writing to a H264 file. This is playable via the `omxplayer` we used earlier.

```bash
pi$ sudo python camera_video_pi.py
```

## Section 2: IoT Edge

### What is Azure IoT Edge

Azure IoT Edge moves cloud analytics and custom business logic to devices so that your organization can focus on business insights instead of data management. Analytics drives business value in IoT solutions, but not all analytics needs to be in the cloud. If you want a device to respond to emergencies as quickly as possible, you can perform anomaly detection on the device itself, or custom business code and machine learning models. 

Azure IoT Edge is made up of three components:
* IoT Edge modules are containers that run Azure services (e.g. Machine Learning, Stream Analytics), 3rd party services, or your own code (e.g. Python, C#). They are deployed to IoT Edge devices and execute locally on those devices. 
* The IoT Edge runtime runs on each IoT Edge device and manages the modules deployed to each device. 
* A cloud-based interface enables you to remotely monitor and manage IoT Edge devices.

### IoT Edge runtime

The Azure IoT Edge runtime enables custom and cloud logic on IoT Edge devices. It sits on the IoT Edge device, and performs management and communication operations. The runtime performs several functions:

* Installs and updates workloads on the device.
* Maintains Azure IoT Edge security standards on the device.
* Ensures that IoT Edge modules are always running.
* Reports module health to the cloud for remote monitoring.
* Facilitates communication between downstream leaf devices and the IoT Edge device.
* Facilitates communication between modules on the IoT Edge device.
* Facilitates communication between the IoT Edge device and the cloud.

![IoT Edge runtime sends insights and reporting to IoT Hub](https://docs.microsoft.com/en-us/azure/iot-edge/media/how-iot-edge-works/runtime.png)

How you use an Azure IoT Edge device is completely up to you. The runtime is often used to deploy AI to gateways which aggregate and process data from multiple other on premises devices, however this is just one option. Leaf devices could also be Azure IoT Edge devices, regardless of whether they are connected to a gateway or directly to the cloud.

![Telemetry, insights, and actions of devices are coordinated with the cloud](https://docs.microsoft.com/en-us/azure/iot-edge/media/how-iot-edge-works/cloud-interface.png)

## Deploy to the Edge

We will setup the Raspberry Pi as a IoT Edge Device.

### Prerequisites

* Have an IoT Hub service provisioned in Azure.
* `pip`

## Install Docker on Raspberry Pi

The IoT Edge control will be installed `pip`, however, it uses Docker containers for setting up services and modules. We will install Docker on the Raspberry Pi.

For simplicity, I have moved this to a Bash install script. You are welcome to review the installation instructions before running the script at [https://docs.docker.com/install/linux/docker-ce/debian/](https://docs.docker.com/install/linux/docker-ce/debian/)

```bash
~/week8:pi$ ./install_docker_pi.sh
```

You should see a "Hello from Docker!" message from a hello-world container, similar to what happened on the Mac/Windows/Linux.

### Register an IoT Edge device

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/register-device.png)

Create a device identity for your simulated device so that it can communicate with your IoT hub. Since IoT Edge devices behave and can be managed differently than typical IoT devices, you declare this to be an IoT Edge device from the beginning. 

1. In the Azure portal, navigate to your IoT hub.
1. Select **IoT Edge (preview)** then select **Add IoT Edge Device**.

   ![Add IoT Edge Device](https://docs.microsoft.com/en-us/azure/includes/media/iot-edge-register-device/add-device.png)

1. Give your simulated device a unique device ID.
1. Select **Save** to add your device.
1. Select your new device from the list of devices.
1. Copy the value for **Connection string—primary key** and save it. You'll use this value to configure the IoT Edge runtime in the next section. 

### Install and start the IoT Edge runtime

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/start-runtime.png)

The IoT Edge runtime is deployed on all IoT Edge devices. It comprises two modules. First, the IoT Edge agent facilitates deployment and monitoring of modules on the IoT Edge device. Second, the IoT Edge hub manages communications between modules on the IoT Edge device, and between the device and IoT Hub.

This is the official way to install our app:

```bash
~/week8:pi$ sudo pip install -U azure-iot-edge-runtime-ctl
```

Let's setup our device as an IoT Edge device. This will allow us to setup the services that will work with IoT Hub.

```bash
~/week8:pi$ sudo iotedgectl setup --connection-string "<DEVICE_CONNECTION_STRING>" --nopass
```

Start the runtime:

```bash
~/week8:pi$ sudo iotedgectl start
```


**OPTIONAL SECTION IF ABOVE DIDN'T WORK**

That didn't work for me so I containerized the application and it can be found by running the following:

```bash
~/week8:pi$ ./start_iotedgectl_container.sh
```

Let's setup our device as an IoT Edge device. This will allow us to setup the services that will work with IoT Hub.

```bash
docker$ iotedgectl setup --connection-string "<DEVICE_CONNECTION_STRING>" --nopass
```

Start the runtime:

```bash
docker$ iotedgectl start
```

**END OPTIONAL SECTION**



Check Docker to see that the IoT Edge agent is running as a module:

```bash
pi$ sudo docker ps
```

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-simulate-device-linux/docker-ps.png)

## Section 3: 

### How to build a classifier with Custom Vision

To use the Custom Vision Service, you must first build a classifier. We will learn to build a classifier through your web browser.

#### Prerequisites

To build a classifier, you must first have:

- A valid [Microsoft account](https://account.microsoft.com/account) or an Azure Active Directory OrgID ("work or school account"), so you can sign into [customvision.ai](http://customvision.ai) and get started.

- A series of images to train your classifier (with a minimum of 30 images per tag).
  - **NOTE**: This can be trained with 10 images with less accurate results.

- A few images to test your classifier after the classifier is trained.

- Optional: An Azure subscription associated with your Microsoft Account or OrgID. If you don’t have an Azure subscription, you can create a [free account](https://azure.microsoft.com/free/?WT.mc_id=A261C142F) before you begin.

## Create a new project

To create a new project, use the following steps:

1. In your web browser, navigate to the [Custom Vision web page](https://customvision.ai). Select __Sign in__ to begin using the service.

    ![Image of the sign-in page](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/custom-vision-web-ui.png)

    > [!NOTE]
    > After you sign in to Custom Vision Service, you are presented with a list of projects. Outside of two "limited trial" projects for testing, projects are associated with an Azure Resource. Since you are an Azure user, you will see all the projects associated with [Azure Resources](https://docs.microsoft.com/azure/guides/developer/azure-developer-guide#grant-access-to-resources) to which you have access. 

2. To create your first project, select **New Project**. For your first project, you are asked to agree to the Terms of Service. Select the check box, and then select the **I agree** button. The **New project** dialog box appears.

    ![The new project dialog box has fields for name, description, and domains.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/new-project.png)

3. Enter a name and a description for the project. Then select one of the available domains. Each domain optimizes the classifier for specific types of images, as described in the following table:

    |Domain|Purpose|
    |---|---|
    |__Generic__| Optimized for a broad range of image classification tasks. If none of the other domains are appropriate, or you are unsure of which domain to choose, select the Generic domain. |
    |__Food__|Optimized for photographs of dishes as you would see them on a restaurant menu. If you want to classify photographs of individual fruits or vegetables, use the Food domain.|
    |__Landmarks__|Optimized for recognizable landmarks, both natural and artificial. This domain works best when the landmark is clearly visible in the photograph. This domain works even if the landmark is slightly obstructed by people in front of it.|
    |__Retail__|Optimized for images that are found in a shopping catalog or shopping website. If you want high precision classifying between dresses, pants, and shirts, use this domain.|
    |__Adult__|Optimized to better define adult content and non-adult content. For example, if you want to block images of people in bathing suits, this domain allows you to build a custom classifier to do that.|
    |__Compact domains__| Optimized for the constraints of real-time classification on mobile devices. The models generated by compact domains can be exported to run locally.|

    You can change the domain later if you want.

4. Select a Resource Group. The Resource Group dropdown shows you all of your Azure Resource Groups that include a Custom Vision Service Resource. You can also create select __limited trial__. The limited trial entry is the only resource group a non-Azure user will be able to choose from.

    To create the project, select __Create project__.

## Upload and tag images

1. To add images to the classifier, use the __Add images__ button and then select __Browse local files__. Select __Open__ to move to tagging.

    > [!TIP]
    > After selecting images, you must tag them. The tag is applied to the group of images you have selected to upload, so it may be easier to upload images by the tags you plan to use. You can also change the tag for selected images after they have been tagged and uploaded.

    > [!TIP]
    > Upload images with different camera angles, lighting, background, types, styles, groups, sizes, etc. Use a variety of photo types to ensure that your classifier is not biased  and can generalize well.

    Custom Vision Service accepts training images in .jpg, .png, and .bmp format, up to 6 MB per image. (Prediction images can be up to 4 MB per image.) We recommend that images be 256 pixels on the shortest edge. Any images shorter than 256 pixels on the shortest edge are scaled up by Custom Vision Service.

    ![The add images control is shown in the upper left, and as a button at bottom center.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/add-images01.png)

    ![The browse local files button is shown near bottom center.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/add-images02.png)

    >[!NOTE] 
    > The REST API can be used to load training images from URLs.

2. To set the tag, enter text in the __My Tags__ field and then use the __+__ button. To upload the images and tag them, use the __Upload [number] files__ button. You can add more than one tag to the images. 

    > [!NOTE]
    > The upload time varies by the number and size of images you have selected.

    ![Image of the tag and upload page](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/add-images03.png)

3. Select __Done__ once the images have been uploaded.

    ![The progress bar shows all tasks completed.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/add-images04.png)

4. To upload another set of images, return to step 1. For example, if you want to distinguish between dogs and ponies, upload and tag images of ponies.

## Train and evaluate the classifier

To train the classifier, select the **Train** button.

![The train button is near the right top of the browser window.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/train02.png)

It only takes a few minutes to train the classifier. During this time, information about the training process is displayed.

![The train button is near the right top of the browser window.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/train03.png)

After training, the __Performance__ is displayed. The precision and recall indicators tell you how good your classifier is, based on automatic testing. Custom Vision Service uses the images that you submitted for training to calculate these numbers, by using a process called [k-fold cross validation](https://en.wikipedia.org/wiki/Cross-validation_(statistics)).

![The training results show the overall precision and recall, and the precision and recall for each tag in the classifier.](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/getting-started-build-a-classifier/train03.png)

> [!NOTE] 
> Each time you select the **Train** button, you create a new iteration of your classifier. You can view all your old iterations in the **Performance** tab, and you can delete any that might be obsolete. When you delete an iteration, you end up deleting any images that are uniquely associated with it.

The classifier uses all the images to create a model that identifies each tag. To test the quality of the model, the classifier tries each image on the model to see what the model finds.

The qualities of the classifier results are displayed.

### Export your classifer

Custom Vision Service allows classifiers to be exported to run offline. You can embed your exported classifier into an application and run it locally on a device for real-time classification. 

Custom Vision Service supports the following exports:

* __Tensorflow__ for __Android__.
* __CoreML__ for __iOS11__.
* __ONNX__ for __Windows ML__.
* A Windows or Linux __container__. The container includes a Tensorflow model and service code to use the Custom Vision Service API. 

> [!IMPORTANT]
> Custom Vision Service only exports __compact__ domains. The models generated by compact domains are optimized for the constraints of real-time classification on mobile devices. Classifiers built with a compact domain may be slightly less accurate than a standard domain with the same amount of training data.
>
> For information on improving your classifiers, see the [Improving your classifier](getting-started-improving-your-classifier.md) document.

## Convert to a compact domain

> [!NOTE]
> The steps in this section only apply if you have an existing classifier that is not set to compact domain.
 
To convert the domain of an existing classifier, use the following steps:

1. From the [Custom vision page](https://customvision.ai), select the __Home__ icon to view a list of your projects. You can also use the [https://customvision.ai/projects](https://customvision.ai/projects) to see your projects.

    ![Image of the home icon and projects list](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/export-your-model/projects-list.png)

2. Select a project, and then select the __Gear__ icon in the upper right of the page.

    ![Image of the gear icon](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/export-your-model/gear-icon.png)

3. In the __Domains__ section, select a __compact__ domain. Select __Save Changes__ to save the changes.

    ![Image of domains selection](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/export-your-model/domains.png)

4. From the top of the page, select __Train__ to retrain using the new domain.

## Export your model

To export the model after retraining, use the following steps:

1. Go to the **Performance** tab and select __Export__. 

    ![Image of the export icon](https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/media/export-your-model/export.png)

    > [!TIP]
    > If the __Export__ entry is not available, then the selected iteration does not use a compact domain. Use the __Iterations__ section of this page to select an iteration that uses a compact domain, and then select __Export__.

2. Select the export format, we will be using **Android / TensorFlow**, and then select __Export__ to download the model.

---

## Section 4: Wiring it up!

One of the key capabilities of Azure IoT Edge is being able to deploy modules to your IoT Edge devices from the cloud. An IoT Edge module is an executable package implemented as a container. 

![](https://docs.microsoft.com/en-us/azure/iot-edge/media/tutorial-install-iot-edge/deploy-module.png)

### Add your ML model to the CustomVisionContainer

#### Export your model from the Custom Vision service and open it

After training an image set, select **Export** and choose **Android / TensorFlow**. Rename `labels.txt` to `model.pbtxt` and replace the `model.pb` and `model.pbtxt` files in the CustomVisionContainer with the generated entities. 

Once you've done this add the files (`model.pb` and `model.pbtxt`) under the folder `~/week8/CustomVisionContainer/models_classify/`

As shown here:

```
|-week8
 |-CustomVisionContainer
  |-models_classify
   |-model.pb
   |-model.pbtxt
```

#### Let's build the Docker images and push them to DockerHub:

First build the CustomVisionContainer (please update <DOCKERHUB_USERNAME> to your username that you use at Docker Hub):

Notice that I am also added a tag: **0.0.1-arm32v7**

It is recommended to always add a tag to differentiate version and target platforms. In this case, its our _0.0.1_ version for an _arm32v7_ platform (aka suitable for the Pi).

```bash
~/week8:pi$ cd CustomVisionContainer
~/CustomVisionContainer:pi$ docker build --file Dockerfile.arm32v7 -t <DOCKERHUB_USERNAME>/customvisionmodule:0.0.1-arm32v7 .
```

Then build the StartupContainer:

```bash
~/week8:pi$ cd StartupContainer
~/StartupContainer:pi$ docker build --file Dockerfile.arm32v7 -t <DOCKERHUB_USERNAME>/startupmodule:0.0.1-arm32v7 .
```

Let's upload these containers:

```bash
~/week8:pi$ docker push <DOCKERHUB_USERNAME>/startupmodule:0.0.1-arm32v7
~/week8:pi$ docker push <DOCKERHUB_USERNAME>/customvisionmodule:0.0.1-arm32v7
```

**NOTE:** As you update your Docker images, for instance changing code or ML model, you're going to have to rebuild the image and modify the tag/version (e.g. 0.0.1 to 0.0.2). After that, you'll need to push these new images back to Docker Hub. After that you'll updated the following file `deployment.json` to reflect the new Docker image. 

Alternatively, this can be done faster using [Azure IoT Edge Extension for VSCode](https://marketplace.visualstudio.com/items?itemName=vsciot-vscode.azure-iot-edge)

### Update deployment.json

In the following areas, ensure you are referencing your own Docker Image (e.g. `<YOUR_DOCKERHUBER_USERNAME>`) that is pushed to Docker Hub.

```json
{
    "StartupContainer": {
    "version": "1.0",
    "type": "docker",
    "status": "running",
    "restartPolicy": "always",
    "settings": {
        "image": "<YOUR_DOCKERHUBER_USERNAME>/startupmodule:0.0.5-arm32v7",
        "createOptions": "{\"Env\": [\"DEVICE=RPI\", \"AZURE_ML_HOSTNAME=CustomVisionContainer\"],\"HostConfig\": { \"Binds\": [\"/home/pi/image:/home/pi/image\", \"/dev:/dev\", \"/etc/RTIMULib.ini:/etc/RTIMULib.ini\"],  \"Privileged\": true, \"PortBindings\":{\"8082/tcp\":[{\"HostPort\":\"8082\"}]}}}"
    }
    },
    "CustomVisionContainer": {
    "version": "1.0",
    "type": "docker",
    "status": "running",
    "restartPolicy": "always",
    "settings": {
        "image": "<YOUR_DOCKERHUBER_USERNAME>/customvisionmodule:0.0.7-arm32v7",
        "createOptions": "{\"Env\": [\"STARTUP_CONTAINER_HOSTNAME=StartupContainer\"],\"HostConfig\":{\"PortBindings\":{\"8080/tcp\":[{\"HostPort\":\"8080\"}]}}}"
    }
}
```

### Apply the configuration to your IoT Edge device:

For details about the deployment file, please review the docs at: [module composition](https://docs.microsoft.com/en-us/azure/iot-edge/module-composition)

The file `deployment.json` describes all the modules we want our IoT Edge device to run.

You can run the following command on your local machine, or the Pi, wherever the `az` (Azure CLI) command is available. You can use the Azure Cloud Shell, however you will need to download the deployment.json file there for it to run.

```bash
az iot hub apply-configuration --device-id <YOUR_DEVICE_EDGE_NAME> --hub-name <YOUR_HUB_NAME> --content deployment.json
```

### View modules

Confirm that the modules deployed from the cloud is running on your IoT Edge device:

```bash
pi# sudo docker ps
```

### Tail logs to show probability of ML face.

```
pi$ docker logs -f CustomVisionContainer
```

You should see output like:

```
category:  {0: 'Gordon\n', 1: 'Leo\n'}
result:  {'class': 'Leo', 'confidence': '0.9884209535'}
172.18.0.5 - - [01/Jun/2018 02:53:14] "POST /classify HTTP/1.1" 200 -
I'm here
{'class': 'Leo', 'confidence': '0.0084209535'}
http://172.18.0.5:8082/yes
Evaluating image with model model
1
category:  {0: 'Gordon\n', 1: 'Leo\n'}
result:  {'class': 'Leo', 'confidence': '0.009765442'}
172.18.0.5 - - [01/Jun/2018 02:53:15] "POST /classify HTTP/1.1" 200 -
I'm here
{'class': 'Leo', 'confidence': '0.009765442'}
http://172.18.0.5:8082/no
Evaluating image with model model
1
```

### Troubleshooting

If containers aren't spinning up, check the logs on the `edgeAgent` container, which is responsible for starting/stopping containers.

```
pi$ docker logs -f edgeAgent
```

The containers we are deploying will require 80/443 ports to be open on the Raspberry Pi. Previous classes may have kept an NGINX service running on 80/443 ports, you can stop and disable this NGINX by doing the following:

```
pi$ sudo service nginx stop
pi$ sudo systemctl disable nginx
```

### Motion Detector

You'll need a Passive Infrared (PIR) motion sensor to continue, you can follow this guide to get you on your way: [https://www.raspberrypi.org/learning/parent-detector/worksheet/](https://www.raspberrypi.org/learning/parent-detector/worksheet/)