#### Remove old docker versions (if installed). It’s OK if apt-get reports that none of these packages are installed.
sudo apt-get remove -y docker docker-engine docker.io

### Update repos for docker

# Update the apt package index
sudo apt-get update -y

# Install packages to allow apt to use a repository over HTTPS
sudo apt-get install -y \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common

# Add Docker official GPG key
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

# Verify fingerprint
sudo apt-key fingerprint 0EBFCD88

# Use the following command to set up the stable repository.
# This is specific to armhf
echo "deb [arch=armhf] https://download.docker.com/linux/debian \
     $(lsb_release -cs) stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list


#### Install docker
# Update the apt package index
sudo apt-get update -y

# Install the latest version of Docker CE
sudo apt-get install -y docker-ce

# Verify it runs
sudo docker run hello-world

# Remove docker-compose if install
sudo pip uninstall docker-compose