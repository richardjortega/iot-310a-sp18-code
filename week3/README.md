# Week 3: Device Connectivity and Management

The following content is partially taken from [https://cloud.google.com/solutions/iot-overview](https://cloud.google.com/solutions/iot-overview) and is modified here under the [Creative Commons Attribution 3.0 License](https://creativecommons.org/licenses/by/3.0/).

## Overview of Internet of Things

Internet of Things (IoT) is a sprawling set of technologies and use cases that has no clear, single definition. One workable view frames IoT as the use of network-connected devices, embedded in the physical environment, to improve some existing process or to enable a new scenario not previously possible.

These devices, or things, connect to the network to provide information they gather from the environment through sensors, or to allow other systems to reach out and act on the world through actuators. They could be connected versions of common objects you might already be familiar with, or new and purpose-built devices for functions not yet realized. They could be devices that you own personally and carry with you or keep in your home, or they could be embedded in factory equipment, or part of the fabric of the city you live in. Each of them is able to convert valuable information from the real world into digital data that provides increased visibility into how your users interact with your products, services, or applications.

The specific use cases and opportunities across different industries are numerous, and in many ways the world of IoT is just getting started. What emerges from these scenarios is a set of common challenges and patterns. IoT projects have additional dimensions that increase their complexity when compared to other cloud-centric technology applications, including:

* Diverse hardware
* Diverse operating systems and software on the devices
* Different network gateway requirements

## Overview of the top-level components of device connectivity

Here we divide the system into three basic high-level areas:

  * Device (IP capable and non-IP capable)
  * Device Gateway (Optional)
  * Cloud Gateway

Included below is a high-level IoT reference architecture that details how Device Connectivity plays a role:

![](./assets/iot-reference-architecture.png)

A *device* includes hardware and software that directly interact with the world. Devices connect to a network to communicate with each other, or to centralized applications. Devices might be directly or indirectly connected to the internet, specifically this means whether the device is  IP-capable or not.

A *device gateway* enables devices that are not directly connected to the Internet (i.e not IP-capable) to reach cloud services. Although the term gateway has a specific function in networking, it is also used to describe a class of device that processes data on behalf of a group or cluster of devices. The data from each device is sent to Cloud Platform, where it is processed and combined with data from other devices, and potentially with other business-transactional data. A device gateway may also convert protocols as needed (e.g. MQTT to AMQP or Bluetooth/custom to IP).

A *cloud gateway* is a service hosted in the cloud (or running on a closed network server) that receives information from devices and is responsible for handling ingress of device data and maintaining connections. The cloud gateway is typically not aware of application payloads nor does it typically handle computing; however, there are connections and integrations to other services.

![](./assets/hubarchitecture.png)

## IoT device-connectivity challenges

IoT devices:

* Are often embedded systems with no human operator.
* Can be in remote locations, where physical access is expensive.
* May only be reachable through the solution back end.
* May have limited power and processing resources.
* May have intermittent, slow, or expensive network connectivity.
* May need to use proprietary, custom, or industry-specific application protocols.
* Can be created using a large set of popular hardware and software platforms.

In addition to the requirements above, any IoT solution must also deliver scale, security, and reliability. The resulting set of connectivity requirements is hard and time-consuming to implement when you use traditional technologies, such as web containers and messaging brokers.

## IoT connectivity and device management offerings

 Most major cloud providers have a managed cloud platform that lets connected devices easily and securely interact with cloud applications and other devices. These are a suite of related IoT products and services to help build the plumbing for your IoT solution. For now, we'll only focus on connectivity and device management, edge computing and services will come later.

* Amazon AWS
  * [IoT Core](https://aws.amazon.com/iot-core/)
  * [IoT Device Management](https://aws.amazon.com/iot-device-management/)
* Microsoft Azure
  * [IoT Hub](https://azure.microsoft.com/en-us/services/iot-hub/)
    * Handles connectivity and device management
    * [Tier Comparison](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-scaling)
* Google Cloud Platform
  * [IoT Core](https://cloud.google.com/iot-core/)
    * Handles connectivity and device management

We will be using Azure IoT Hub for this course.

## What we'll be doing

We'll do a few things in the portal to keep it easy, but this can all be doing programmatically through the Azure CLI or REST API.

* Create a Resource Group to hold everything
* Create an IoT hub.
* Register a device for Pi in your IoT hub.
* Configure Raspberry Pi.
* Run a sample application on Pi to send sensor data to your IoT hub.
* Add our own application to send SenseHAT data

## Create a resource group (Azure Portal)

A resource group is a container that holds related resources for an Azure solution. The resource group can include all the resources for the solution, or only those resources that you want to manage as a group. You decide how you want to allocate resources to resource groups based on what makes the most sense for your organization. Generally, add resources that share the same lifecycle to the same resource group so you can easily deploy, update, and delete them as a group.

The resource group stores metadata about the resources. Therefore, when you specify a location for the resource group, you are specifying where that metadata is stored. For compliance reasons, you may need to ensure that your data is stored in a particular region.

1. To see all the resource groups in your subscription, select **Resource groups**.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/browse-groups.png)

2. To create an empty resource group, select **Add**.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/add-resource-group.png)

3. Provide a name and location for the new resource group. Select **Create**.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/create-empty-group.png)

4. You may need to select **Refresh** to see the recently created resource group.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/refresh-resource-groups.png)

5. To customize the information displayed for your resource groups, select **Columns**.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/select-columns.png)

6. Select the columns to add, and then select **Update**.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/add-columns.png)

7. To learn about deploying resources to your new resource group, see [Deploy resources with Resource Manager templates and Azure portal](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-template-deploy-portal).

8. For quick access to a resource group, you can pin the resource group to your dashboard.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/pin-group.png) 

9. The dashboard displays the resource group and its resources. You can select either the resource groups or any of its resources to navigate to the item.

![](https://docs.microsoft.com/en-us/azure/azure-resource-manager/media/resource-group-portal/show-resource-group-dashboard.png)

10. You can apply tags to resource groups and resources to logically organize your assets. For information about working with tags, see [Using tags to organize your Azure resources](https://docs.microsoft.com/en-us/azure/azure-resource-manager/resource-group-using-tags.

## Create an IoT hub

1. Sign in to the [Azure portal][lnk-portal].
1. Select **Create a resource** > **Internet of Things** > **IoT Hub**.
   
    ![](https://docs.microsoft.com/en-us/azure/includes/media/iot-hub-create-hub/create-iot-hub1.png)

1. In the **IoT hub** pane, enter the following information for your IoT hub:

   * **Name**: Create a name for your IoT hub. If the name you enter is valid, a green check mark appears.

  ```
  The IoT hub will be publicly discoverable as a DNS endpoint, so make sure to avoid any sensitive information while naming it.
  ```

   * **Pricing and scale tier**: For this tutorial, select the **F1 - Free** tier. For more information, see the [Pricing and scale tier][lnk-pricing].

   * **Resource group**: Create a resource group to host the IoT hub or use an existing one. For more information, see [Use resource groups to manage your Azure resources][lnk-resource-groups]

   * **Location**: Select the closest location to you.

   * **Pin to dashboard**: Check this option for easy access to your IoT hub from the dashboard.

    ![IoT hub window](https://docs.microsoft.com/en-us/azure/includes/media/iot-hub-create-hub/create-iot-hub2.png)

1. Click **Create**. Your IoT hub might take a few minutes to create. You can monitor the progress in the **Notifications** pane.

Now that you have created an IoT hub, locate the important information that you use to connect devices and applications to your IoT hub. 

1. After your IoT hub is created, click it on the dashboard. Make a note of the **Hostname**, and then click **Shared access policies**.

   ![Get the hostname of your IoT hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-create-hub-and-device/4_get-azure-iot-hub-hostname-portal.png)

1. In the **Shared access policies** pane, click the **iothubowner** policy, and then copy and make a note of the **Connection string** of your IoT hub. For more information, see [Control access to IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-devguide-security).

```
You will not need this iothubowner connection string for this set-up tutorial. However, you may need it for some of the tutorials on different IoT scenarios after you complete this set-up.
```

   ![Get your IoT hub connection string](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-create-hub-and-device/5_get-azure-iot-hub-connection-string-portal.png)

## Register a device in the IoT hub for your device

1. In the [Azure portal](https://portal.azure.com/), open your IoT hub.

2. Click **IoT Devices**.
3. In the IoT Devices pane, click **Add** to add a device to your IoT hub. Then do the following:

   **Device ID**: Enter the ID of the new device. Device IDs are case sensitive.

   **Authentication Type**: Select **Symmetric Key**.

   **Auto Generate Keys**: Select this check box.

   **Connect device to IoT Hub**: Click **Enable**.

   ![Add a device in the Device Explorer of your IoT hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-create-hub-and-device/6_add-device-in-azure-iot-hub-device-explorer-portal.png)

```
NOTE: The device ID may be visible in the logs collected for customer support and troubleshooting, so make sure to avoid any sensitive information while naming it.
```

4. Click **Save**.
5. After the device is created, open the device in the **IoT Devices** pane.
6. Make a note of the primary key of the connection string.

   ![Get the device connection string](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-create-hub-and-device/7_get-device-connection-string-in-device-explorer-portal.png)


## Run a Python Client App on Raspberry Pi

We will first download a sample application, configure it, and run simulated data. This client app will give us a skeleton for connecting to IoT Hub.

### Configure the sample application using SenseHat

1. Clone the sample application by running the following command:

   ```bash
   # You can clone to your Home folder, or wherever you perfer.
   pi$ cd ~
   pi$ git clone https://github.com/richardjortega/rpi-client-sensehat
   ```
1. Open the config file by running the following commands:

   ```bash
   pi$ cd iot-hub-python-raspberrypi-client-app
   # Or your favorite editor
   pi$ nano config.py
   ```

   There are 5 macros in this file you can configure. The first one is `MESSAGE_TIMESPAN`, which defines the time interval (in milliseconds) between two messages that send to cloud. The second one `SIMULATED_DATA`, which is a Boolean value for whether to use simulated sensor data or not. 
   
   There are other settings for I2C and GPIO access we will not focus on, because this examples uses a BME280 sensor. For instance, `I2C_ADDRESS` is the I2C address which the BME280 sensor is connected. `GPIO_PIN_ADDRESS` is the GPIO address for an LED. The last one is `BLINK_TIMESPAN`, which defined the timespan when your LED is turned on in milliseconds.

   Since you **may or may not** have the sensor, set the `SIMULATED_DATA` value to `True` to make the sample application create and use simulated sensor data. We will integrate the SenseHAT in class together.

1. Save and exit by pressing Control-O > Enter > Control-X.

### Build and run the sample application

1. Build the sample application by running the following command. Because the Azure IoT SDKs for Python are wrappers on top of the Azure IoT Device C SDK, you will need to compile the C libraries if you want or need to generate the Python libraries from source code.

   ```bash
   sudo chmod u+x setup.sh
   sudo ./setup.sh
   ```
   
   **TROUBLESHOOTING NOTES**

   *You can also specify the version you want by running `sudo ./setup.sh [--python-version|-p] [2.7|3.4|3.5]`. If you run script without parameter, the script will automatically detect the version of python installed (Search sequence 2.7->3.4->3.5). Make sure your Python version keeps consistent during building and running.*
   
   *On building the Python client library (iothub_client.so) on Linux devices that have less than 1GB RAM, you may see build getting stuck at 98% while building iothub_client_python.cpp as shown below `[ 98%] Building CXX object python/src/CMakeFiles/iothub_client_python.dir/iothub_client_python.cpp.o`. If you run into this issue, check the memory consumption of the device using `free -m command` in another terminal window during that time. If you are running out of memory while compiling iothub_client_python.cpp file, you may have to temporarily increase the swap space to get more available memory to successfully build the Python client-side device SDK library.*
   
1. Run the sample application by running the following command:

   ```bash
   python app.py '<your Azure IoT hub device connection string>'
   ```

   *Make sure you copy-paste the device connection string into the single quotes. And if you use the python 3, then you can use the command `python3 app.py '<your Azure IoT hub device connection string>'`.*


   You should see the following output that shows the sensor data and the messages that are sent to your IoT hub.

   ![Output - sensor data sent from Raspberry Pi to your IoT hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-raspberry-pi-kit-c-get-started/success.png)

## Listening to Messages (or sending messages to Device)

You’ve run a sample application to collect sensor data and send it to your IoT hub. To see the messages that your Raspberry Pi has sent to your IoT hub or send messages to your Raspberry Pi in a command line interface we can use: [iothub-explorer](https://github.com/azure/iothub-explorer). 

**NOTE**: Alternatively, we can use an extension for VSCode: [Azure IoT Toolkit for VSCode](https://marketplace.visualstudio.com/items?itemName=vsciot-vscode.azure-iot-toolkit). This will be demoed in class.



### Install iothub-explorer

[iothub-explorer](https://github.com/azure/iothub-explorer) has a handful of commands that makes IoT Hub management easier. This tutorial focuses on how to use iothub-explorer to send and receive messages between your device and your IoT hub. 

It is a CLI tool to manage device identities in your IoT hub registry, send and receive messages and files from your devices, and monitor your IoT hub operations.

iothub-explorer also lets you simulate a device connected to your IoT hub.

**NOTE** This tool requires Node.js version 4.x or higher for all features to work.

To install the latest version of the iothub-explorer tool (can be on host or pi):

```
$ npm install -g iothub-explorer

# To get help use:
$ iothub-explorer help
```


### Monitor device-to-cloud messages

To monitor messages that are sent from your device to your IoT hub, follow these steps:

1. Open a console window.
1. Run the following command:

   ```bash
   iothub-explorer monitor-events <device-id> --login "<IoTHubConnectionString>"
   ```

   **NOTE**: Get `<device-id>` and `<IoTHubConnectionString>` from your IoT hub. Make sure you've finished the previous tutorial. Or you can try to use `iothub-explorer monitor-events <device-id> --login "HostName=<my-hub>.azure-devices.net;SharedAccessKeyName=<my-policy>;SharedAccessKey=<my-policy-key>"` if you have `HostName`, `SharedAccessKeyName` and `SharedAccessKey`.

### Send cloud-to-device messages

To send a message from your IoT hub to your device, follow these steps:

1. Open a console window.
1. Start a session on your IoT hub by running the following command:

   ```bash
   iothub-explorer login `<IoTHubConnectionString>`
   ```

1. Send a message to your device by running the following command:

   ```bash
   iothub-explorer send <device-id> <message>
   ```

The command blinks the LED that is connected to your device and sends the message to your device.

**NOTE:** There is no need for the device to send a separate ack command back to your IoT hub upon receiving the message.

### Working with device identity registry

Supply your IoT hub connection string once using the **login** command. This means you do not need to supply the connection string for subsequent commands for the duration of the session (defaults to one hour):

```shell
$ iothub-explorer login "HostName=<my-hub>.azure-devices.net;SharedAccessKeyName=<my-policy>;SharedAccessKey=<my-policy-key>"

Session started, expires Fri Jan 15 2016 17:00:00 GMT-0800 (Pacific Standard Time)
```

To retrieve information about an already-registered device from the device identity registry in your IoT hub, including the device connection string, use the following command:

```shell
$ iothub-explorer get known-device --connection-string

-
  deviceId:                   known-device
  ...
-
  connectionString: HostName=<my-hub>.azure-devices.net;DeviceId=known-device;SharedAccessKey=<known-device-key>
```

To register a new device and get its connection string from the device identity registry in your IoT hub, use the following command:

```shell
$ iothub-explorer create new-device --connection-string

Created device new-device

-
  deviceId:                   new-device
  ...
-
  connectionString: HostName=<my-hub>.azure-devices.net;DeviceId=new-device;SharedAccessKey=<new-device-key>
```

To delete a device from the device identity registry in your IoT hub, use the following command:

```shell
$ iothub-explorer delete existing-device
```

## As a Class / On Your Own

* Integrate SenseHAT app to talk to IoT Hub.
* Connect a client to listen for updates to IoT Hub.
  * You could connect your Electron app to listen for messages.