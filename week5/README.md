# Week 5: Containers Continued and Serverless, Databases, Storage

![](./assets/iot-ref-arch.png)

This week's class will integrate the components based on the following diagram:

![](./assets/week5-architecture.png)
## Create your first container in Azure Container Instances

Azure Container Instances makes it easy to create and manage Docker containers in Azure, without having to provision virtual machines or adopt a higher-level service. In this quickstart, you create a container in Azure and expose it to the internet with a fully qualified domain name (FQDN). This operation is completed in a single command. Within just a few seconds, you'll see this in your browser:

![App deployed using Azure Container Instances viewed in browser](https://docs.microsoft.com/en-us/azure/container-instances/media/container-instances-quickstart/aci-app-browser.png)

**NOTE**: For the following, either use the Cloud Shell or Azure CLI.


## Create a resource group

Azure container instances, like all Azure resources, must be placed in a resource group, a logical collection into which Azure resources are deployed and managed.

Create a resource group with the `az group create` command.

The following example creates a resource group named *myResourceGroup* in the *eastus* location.

```azurecli-interactive
az group create --name myResourceGroup --location eastus
```

## Create a container

You can create a container by providing a name, a Docker image, and an Azure resource group to the `az container create` command. You can optionally expose the container to the internet by specifying a DNS name label. In this quickstart, you deploy a container that hosts a small web app written in [Node.js][node-js].

Execute the following command to start a container instance. The `--dns-name-label` value must be unique within the Azure region you create the instance, so you might need to modify this value to ensure uniqueness.

```azurecli-interactive
az container create --resource-group myResourceGroup --name mycontainer --image microsoft/aci-helloworld --dns-name-label aci-demo --ports 80
```

Within a few seconds, you should get a response to your request. Initially, the container is in the **Creating** state, but it should start within a few seconds. You can check the status using the [az container show][az-container-show] command:

```azurecli-interactive
az container show --resource-group myResourceGroup --name mycontainer --query "{FQDN:ipAddress.fqdn,ProvisioningState:provisioningState}" --out table
```

When you run the command, the container's fully qualified domain name (FQDN) and its provisioning state are displayed:

```console
$ az container show --resource-group myResourceGroup --name mycontainer --query "{FQDN:ipAddress.fqdn,ProvisioningState:provisioningState}" --out table
FQDN                               ProvisioningState
---------------------------------  -------------------
aci-demo.eastus.azurecontainer.io  Succeeded
```

Once the container moves to the **Succeeded** state, you can reach it in your browser by navigating to its FQDN:

![Browser screenshot showing application running in an Azure container instance](https://docs.microsoft.com/en-us/azure/container-instances/media/container-instances-quickstart/aci-app-browser.png)

## Pull the container logs

You can pull the logs for the container you created using the `az container logs` command:

```azurecli-interactive
az container logs --resource-group myResourceGroup --name mycontainer
```

You should see output similar to the following :

```console
$ az container logs --resource-group myResourceGroup -n mycontainer
listening on port 80
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET / HTTP/1.1" 200 1663 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET /favicon.ico HTTP/1.1" 404 150 "http://aci-demo.eastus.azurecontainer.io/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
```

## Attach output streams

In addition to tailing the logs, you can attach your local standard out and standard error streams to that of the container.

First, execute the `az container attach` command to attach your local console the container's output streams:

```azurecli-interactive
az container attach --resource-group myResourceGroup -n mycontainer
```

Once attached, refresh your browser a few times to generate some additional output. Finally, detach your console with `Control+C`. You should see output similar to the following:

```console
$ az container attach --resource-group myResourceGroup -n mycontainer
Container 'mycontainer' is in state 'Running'...
(count: 1) (last timestamp: 2018-03-15 21:17:59+00:00) pulling image "microsoft/aci-helloworld"
(count: 1) (last timestamp: 2018-03-15 21:18:05+00:00) Successfully pulled image "microsoft/aci-helloworld"
(count: 1) (last timestamp: 2018-03-15 21:18:05+00:00) Created container with id 3534a1e2ee392d6f47b2c158ce8c1808d1686fc54f17de3a953d356cf5f26a45
(count: 1) (last timestamp: 2018-03-15 21:18:06+00:00) Started container with id 3534a1e2ee392d6f47b2c158ce8c1808d1686fc54f17de3a953d356cf5f26a45

Start streaming logs:
listening on port 80
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET / HTTP/1.1" 200 1663 "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.105 - - [15/Mar/2018:21:18:26 +0000] "GET /favicon.ico HTTP/1.1" 404 150 "http://aci-demo.eastus.azurecontainer.io/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.107 - - [15/Mar/2018:21:18:44 +0000] "GET / HTTP/1.1" 304 - "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
::ffff:10.240.255.107 - - [15/Mar/2018:21:18:47 +0000] "GET / HTTP/1.1" 304 - "-" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.146 Safari/537.36"
```

## Delete the container

When you are done with the container, you can remove it using the `az container delete` command:

```azurecli-interactive
az container delete --resource-group myResourceGroup --name mycontainer
```

To verify that the container has been deleted, execute the `az container list` command:

```azurecli-interactive
az container list --resource-group myResourceGroup --output table
```

The **mycontainer** container should not appear in the command's output. If you have no other containers in the resource group, no output is displayed.

## Functions and CosmosDB

### Processing data from IoT Hub with Azure Functions

In this quick sample you will learn how to capture data from your devices or sensors, perform aggregation, filtering or some other custom processing on this data, and store it on a database. All this will be done by setting up the integration between IoT Hub and Azure Functions, while learning a bit about triggers and bindings for Azure Functions, and drop your processed data on Cosmos DB.

Ensure you have an IoT Hub created before proceeding.

We will be using the code from Week 3 to provide SenseHAT data from the Raspberry Pi.

```
pi$ git clone git@github.com:richardjortega/rpi-client-sensehat.git
# or for HTTPS version...
# pi$ git clone https://github.com/richardjortega/rpi-client-sensehat.git
```

Remember to:
- Modify `config.py` to your liking
- Have already run the `setup.sh` script
  - `sudo chmod u+x setup.sh`
  - `sudo ./setup.sh`
- `python app.py '<your Azure IoT hub device connection string>'`

### Azure Functions triggered by IoT Hub

Azure Functions has integration with IoT Hub out of the box, so it is very simple to get this up and running. Follow the steps below to get an Azure Function reacting to IoT Hub events:

Create a **Function App** by clicking the below in the Azure Portal “Create New” blade. This is the “container” which holds your functions.

Using the graphic below as a guide, select the following:
- OS: Windows (Linux Preview doesn't currently support Consumption-based Functions)
- Pick the resource group you've been using or create a new one.
- Pick a region that you have been deploying resources to (e.g. eastus, westus)
- For storage, create a new storage for this Function

![](./assets/functions-create.png)

Once the Function is created, navigate to the “Create new function from template page”:

![](./assets/02.png)

Select IoT Hub (Event Hub) and Javascript as the language

![](./assets/03.png)

Conveniently, Azure Functions will create the connection with IoT Hub for you after selecting this template, so you just have to click on the "new" button and select the proper IoT Hub instance:

![](./assets/04.png)

Click "Create" and the Function just created will be triggered by the IoT Hub events

### Cosmos DB creation

Now we need a place to store the events sent to IoT Hub after the processing occur on the Function code, so we followed the steps below to create a Cosmos DB database:

Navigate back to the "Create new" blade in the Azure Portal, and this time select "Cosmos DB"

![](./assets/05.png)

Select SQL as the API to use on Cosmos DB, as it's important to note that Azure Functions only works with Cosmos DB if the SQL or Graph APIs are selected.

That's it! It takes a bit for the resource to be provisioned, so give it a few minutes

### Cosmos DB output binding

Now it's time to hook everything together using the Azure Functions Cosmos DB output binding, and integrate our Function with it following the steps below:

Navigate to the "Integrate" tab of your Azure Function

![](./assets/06.png)

Create a new "Cosmos DB output" binding

![](./assets/07.png)

As with IoT Hub, we have to create a new Cosmos DB Account connection, but again it's quite easy, as you just have to click on "new" and select the proper Cosmos DB account

![](./assets/08.png)

Rather than creating it in Cosmos DB, we opt for the default parameter values and the "Create database and collection for me" option for this sample

![](./assets/09.png)

Now, to modify the code on our Function to store the data on Cosmos DB using the output binding, and since the Function was created in Javascript, all we have to do is add the output to context.bindings

```
module.exports = function (context, IoTHubMessages) {
    context.log(`JavaScript eventhub trigger function called for message array: ${IoTHubMessages}`);

    var count = 0;
    var totalTemperature = 0.0;
    var totalHumidity = 0.0;
    var deviceId = "";

    IoTHubMessages.forEach(message => {
        context.log(`Processed message: ${message}`);
        count++;
        totalTemperature += message.temperature;
        totalHumidity += message.humidity;
        deviceId = message.deviceId;
    });

    var output = {
        "deviceId": deviceId,
        "measurementsCount": count,
        "averageTemperature": totalTemperature/count,
        "averageHumidity": totalHumidity/count
    };

    context.log(`Output content: ${output}`);

    context.bindings.outputDocument = output;

    context.done();
};
```

This will be slightly different if you select a different language (for full reference on how to use Cosmos DB output bindings in Azure Functions see [here](https://docs.microsoft.com/en-us/azure/azure-functions/functions-integrate-store-unstructured-data-cosmosdb))

Note that all we did was to aggregate data calculating average values and output to Cosmos DB, but it's easy to modify the Function code to manipulate the data on any other way before storing on Cosmos DB.

To validate that our Function is being properly triggered and storing the IoT Hub messages, navigate to Cosmos DB and watch the messages being stored

![](./assets/10.png)

## Blob Storage

### Create an Azure storage account

1. In the [Azure portal](https://portal.azure.com/), click **Create a resource** > **Storage** > **Storage account** > **Create**.

2. Enter the necessary information for the storage account:

   ![Create a storage account in the Azure portal](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/1_azure-portal-create-storage-account.png)

   * **Name**: The name of the storage account. The name must be globally unique.

   * **Resource group**: Use the same resource group that your IoT hub uses.

   * **Pin to dashboard**: Select this option for easy access to your IoT hub from the dashboard.

3. Click **Create**.

## Prepare your IoT hub to route messages to storage

IoT Hub natively supports routing messages to Azure storage as blobs. To know more about the Azure IoT Hub custom endpoints, you can refer to [List of built-in IoT Hub endpoints](https://docs.microsoft.com/azure/iot-hub/iot-hub-devguide-endpoints#custom-endpoints).

### Add storage as a custom endpoint

Navigate to your IoT hub in the Azure portal. Click **Endpoints** > **Add**. Name the endpoint and select **Azure Storage Container** as the endpoint type. Use the picker to select the storage account you created in the previous section. Create a storage container and select it, then click **OK**.

  ![Create a custom endpoint in IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/2_custom-storage-endpoint.png)

### Add a route to route data to storage

Click **Routes** > **Add** and enter a name for the route. Select **Device Messages** as the data source, and select the storage endpoint you just created as the endpoint in the route. Enter `true` as the query string, then click **Save**.

  ![Create a route in IoT Hub](https://docs.microsoft.com/en-us/azure/iot-hub/media/iot-hub-store-data-in-azure-table-storage/3_create-route.png)