#!/usr/bin/python
# Inspired from centralEdgeV1-Client (Course 3/Week 1/Lab 1)
# Description: Send message
import time
import socket
import paho.mqtt.client as paho
import os

hostname = os.getenv('HOSTNAME', 'hostname-not-set')
brokerHost = "test.mosquitto.org"     # Local MQTT broker
brokerPort   = 1883          # Local MQTT port
brokerTopic = "iot-310"  # Local MQTT topic to monitor
brokerTimeout = 120          # Local MQTT session timeout

# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print "CONNACK received with code %d." % rc

# Setup to Publish Sensor Data
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.connect(brokerHost, brokerPort, brokerTimeout)

# initialize message dictionary
msg = {'topic':brokerTopic, 'host':hostname, 'time':""}

print 'Broker Details'
print '--------------------------'
print "Broker Host: ", brokerHost
print "Broker Topic: ", brokerTopic
print "Broker Port: ", brokerPort

for i in range(1,9):
    localtime = time.asctime( time.localtime(time.time()) )
    msg['time'] = localtime
    print "Sending local time :", localtime
    mqttc.publish(brokerTopic, str(msg), 0)
    time.sleep(2.0)

print 'End of MQTT Messages'
quit()
